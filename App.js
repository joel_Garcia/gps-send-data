
import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';

export default class App extends Component<{}> {

  constructor(props) {
    super(props);
    this.state = {
      initialPosition: 'loading...'
    };
  }

  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let initialPosition = position.coords;
        // Here the coordinates are passing to the initialPosition object
        this.setState({initialPosition: initialPosition});
        this.send(initialPosition);
      },
      (error) => alert(error.message),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );

  }

  send(initialPosition) {
    fetch('http://192.168.0.95:3000/api/v1/places', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        // the API needs 2 params (latitude,longitude)
        latitude: initialPosition.latitude, // the parameter latitude is within the object initialPosition
        longitude: initialPosition.longitude, // the parameter longitude is within the object initialPosition
      })
    })
  }

  componentDidMount() {
  }

  render() {
    const {initialPosition} = this.state;
    return (
      <View style={{marginTop: 100, marginLeft: 50}}>
        <Text style={{color: '#f00'}}>longitude: { initialPosition.latitude }</Text>
        <Text style={{color: '#f00'}}>longitude: { initialPosition.longitude }</Text>
      </View>
    );
  }
}
